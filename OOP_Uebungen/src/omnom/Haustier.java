package omnom;

public class Haustier {
	int hunger;
	int muede;
	int zufrieden;
	int gesund;
	String name;
	
	
	public Haustier(int hunger, int muede, int zufrieden, int gesund, String name) {
		super();
		this.hunger = hunger;
		this.muede = muede;
		this.zufrieden = zufrieden;
		this.gesund = gesund;
		this.name = name;
	}
	public int getHunger() {
		return hunger;
	}
	public void setHunger(int hunger) {
		this.hunger = hunger;
	}
	public int getMuede() {
		return muede;
	}
	public void setMuede(int muede) {
		this.muede = muede;
	}
	public int getZufrieden() {
		return zufrieden;
	}
	public void setZufrieden(int zufrieden) {
		this.zufrieden = zufrieden;
	}
	public int getGesund() {
		return gesund;
	}
	public void setGesund(int gesund) {
		this.gesund = gesund;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Haustier(String name) {
		super();
		this.hunger = 100;
		this.muede = 100;
		this.zufrieden = 100;
		this.gesund = 100;
		this.name = name;
	}
	public void fuettern(int i) {
		hunger = hunger + 70;
		if (hunger>100) {
			hunger = 100;
		}
		if (hunger < 0) {
			hunger = 0;
		}
		
	}
	public void schlafen(int i) {
		muede = muede + 60;
		if (muede>100) {
			muede = 100;
		}
		if (muede < 0) {
			muede = 0;
		}
		
	}
	public void spielen(int i) {
		zufrieden = zufrieden + 10;
		if (zufrieden>100) {
			zufrieden = 100;
		}
		if (zufrieden < 0) {
			zufrieden = 0;
		}
	}
	public void heilen() {
		this.gesund = 100;
		
	}
	
	
	
}

